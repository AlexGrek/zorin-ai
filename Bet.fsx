#!/bin/fsharpi

(*
Будь-який мурашиний алгоритм, незалежно від модифікацій, представимо в наступному виді 
Acometaheuristic{ 
    Scheduleactivities{ 
    Constructantssolutions(); 
    Updatepheromones(); 
    Evaporate(); 
    Daemonactions();// optional } }
*)

let decode filename =
    filename

let acometaheuristic graph =
    graph

[<EntryPoint>]
let main argv = 
    let file = 
        argv.[1]

    let graph = decode file
    acometaheuristic graph