﻿(*
double gaussian_random(double mue /*mean*/, double sigma/*stsndard deviation*/ ){  double x1, x2, w, y; 
 
 do {   x1 = 2.0 * rand()/(RAND_MAX + 1) - 1.0;   
 x2 = 2.0 * rand()/(RAND_MAX + 1) - 1.0;   
 w = x1 * x1 + x2 * x2;  } while ( w >= 1.0);  
 double llog = log( w ); 
  w = sqrt( (-2.0 * llog ) / w ); 
   y = x1 * w; 
 
 return mue + sigma * y; 
 } 
 *)
let RND = System.Random()
let RAND_MAX = 4.0;

let random() =
    RND.NextDouble()

let levyrnd 

let gaussianRandom mue sigma =
    let mutable x1 =  2.0 * random() / (RAND_MAX + 1.0) - 1.0
    let mutable x2 =  2.0 * random() / (RAND_MAX + 1.0) - 1.0
    let mutable w = x1 * x1 + x2 * x2
    let mutable y = 0.0

    while (w >= 1.0) do
        x1 <- 2.0 * random() / (RAND_MAX + 1.0) - 1.0
        x2 <- 2.0 * random() / (RAND_MAX + 1.0) - 1.0
        w <- x1 * x1 + x2 * x2
        printfn "%A" w

    let llog = log w
    w <- sqrt <| (-2.0 * llog) / w
    let y = x1 * w

    mue + sigma * y

//gaussianRandom 1.0 0.85;;